# Java Lang changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-03-21 13:03.

## Version [2.0.6](https://gitlab.com/ultreiaio/java-lang/-/milestones/18)

**Closed at 2024-03-21.**


### Issues
  * [[enhancement 16]](https://gitlab.com/ultreiaio/java-lang/-/issues/16) **Remove TwoSide API (was moved to java-util project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.5](https://gitlab.com/ultreiaio/java-lang/-/milestones/17)

**Closed at 2023-03-09.**


### Issues
  * [[bug 15]](https://gitlab.com/ultreiaio/java-lang/-/issues/15) **Fix Numbers.round method issues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.4](https://gitlab.com/ultreiaio/java-lang/-/milestones/16)

**Closed at 2021-12-10.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/java-lang/-/issues/14) **Add more methods in Strings** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.3](https://gitlab.com/ultreiaio/java-lang/-/milestones/15)

**Closed at 2021-11-12.**


### Issues
  * [[enhancement 11]](https://gitlab.com/ultreiaio/java-lang/-/issues/11) **Remove ClassLoaders, it won&#39;t work with java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 12]](https://gitlab.com/ultreiaio/java-lang/-/issues/12) **Upgrade to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 13]](https://gitlab.com/ultreiaio/java-lang/-/issues/13) **Introduce TwoSide interface** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.2](https://gitlab.com/ultreiaio/java-lang/-/milestones/14)

**Closed at 2018-12-16.**


### Issues
  * [[enhancement 10]](https://gitlab.com/ultreiaio/java-lang/-/issues/10) **Add method Strings.getRelativeCamelCaseName** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.1](https://gitlab.com/ultreiaio/java-lang/-/milestones/13)

**Closed at 2018-07-30.**


### Issues
  * [[enhancement 9]](https://gitlab.com/ultreiaio/java-lang/-/issues/9) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0](https://gitlab.com/ultreiaio/java-lang/-/milestones/12)

**Closed at *In progress*.**


### Issues
  * [[critical 7]](https://gitlab.com/ultreiaio/java-lang/-/issues/7) **Remove SingletonSupplier (now in java-util library)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[critical 8]](https://gitlab.com/ultreiaio/java-lang/-/issues/8) **Remove JavaBean API (now in java-bean library)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.10](https://gitlab.com/ultreiaio/java-lang/-/milestones/11)

**Closed at *In progress*.**


### Issues
  * [[enhancement 6]](https://gitlab.com/ultreiaio/java-lang/-/issues/6) **Introduce JavaBeanDefinition to avoid any extra techincal codes in JavaBean contract** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.9](https://gitlab.com/ultreiaio/java-lang/-/milestones/10)

**Closed at *In progress*.**


### Issues
  * [[enhancement 5]](https://gitlab.com/ultreiaio/java-lang/-/issues/5) **Introduce AbstractJavaBean and a nice way to generate getters and setters (via annotation processing)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.8](https://gitlab.com/ultreiaio/java-lang/-/milestones/9)

**Closed at *In progress*.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/java-lang/-/issues/1) **Introduce Strings** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 3]](https://gitlab.com/ultreiaio/java-lang/-/issues/3) **Introduce Numbers** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 4]](https://gitlab.com/ultreiaio/java-lang/-/issues/4) **Introduce ClassLoaders** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/java-lang/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.6](https://gitlab.com/ultreiaio/java-lang/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.5](https://gitlab.com/ultreiaio/java-lang/-/milestones/6)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.4](https://gitlab.com/ultreiaio/java-lang/-/milestones/5)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.3](https://gitlab.com/ultreiaio/java-lang/-/milestones/4)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.2](https://gitlab.com/ultreiaio/java-lang/-/milestones/3)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.1](https://gitlab.com/ultreiaio/java-lang/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0.0](https://gitlab.com/ultreiaio/java-lang/-/milestones/1)

**Closed at *In progress*.**


### Issues
No issue.

