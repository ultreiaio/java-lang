# Java Lang extends by Ultreia.io

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/java-lang.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22java-lang%22)
[![Build Status](https://gitlab.com/ultreiaio/java-lang/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/java-lang/pipelines)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

This project offers some class to extends java lang.

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/java-lang/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/java-lang)

# Community

* [Contact](mailto:dev@tchemit.fr)
