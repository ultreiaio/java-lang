package io.ultreia.java4all.lang;

/*-
 * #%L
 * Java Lang extends by Ultreia.io
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Created by tchemit on 03/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("unchecked")
public class Objects2 {

    static final Predicate<PropertyDescriptor> IS_READ_DESCRIPTOR = input -> input.getReadMethod() != null;
    static final Predicate<PropertyDescriptor> IS_WRITE_DESCRIPTOR = input -> input.getWriteMethod() != null;

    private Objects2() {
    }

    public static <O> Class<O> forName(String className) {
        try {
            return (Class<O>) Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(String.format("Can't get class: %s", className), e);
        }
    }

    public static <O> O newInstance(Class<O> type) {
        Objects.requireNonNull(type);
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalStateException(String.format("Can't instantiate type: %s", type.getName()), e);
        }
    }

    public static void walk(ClassWalkVisitor visitor, Class<?> beanType) {
        new ClassWalker(visitor).visit(beanType);
    }

    interface ClassWalkVisitor {

        void onVisit(Class<?> beanType);

        boolean canContinue();
    }

    static class ClassWalker {

        private final ClassWalkVisitor visitor;
        private final Set<Class<?>> exploredTypes;

        ClassWalker(ClassWalkVisitor visitor) {
            this.visitor = visitor;
            this.exploredTypes = new LinkedHashSet<>();
        }

        public void visit(Class<?> beanType) {
            exploredTypes.clear();
            accept(beanType);
        }

        protected boolean accept(Class<?> beanType) {
            if (exploredTypes.contains(beanType)) {

                // already explored
                return visitor.canContinue();
            }
            exploredTypes.add(beanType);

            // get properties for the class
            visitor.onVisit(beanType);
            if (!visitor.canContinue()) {
                return false;
            }
            if (beanType.getSuperclass() != null) {

                // get properties fro super-class
                boolean canContinue = accept(beanType.getSuperclass());
                if (!canContinue) {
                    return false;
                }
            }

            for (Class<?> anInterface : beanType.getInterfaces()) {

                // get properties fro super-class
                boolean canContinue = accept(anInterface);
                if (!canContinue) {
                    return false;
                }
            }
            return visitor.canContinue();
        }
    }
}
