package io.ultreia.java4all.lang;

/*-
 * #%L
 * Java Lang extends by Ultreia.io
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by tchemit on 29/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Numbers {

    public static Float roundOneDigit(Float number) {
        return roundNDigits(number, 1);
    }

    public static Float roundTwoDigits(Float number) {
        return roundNDigits(number, 2);
    }

    public static Float roundThreeDigits(Float number) {
        return roundNDigits(number, 3);
    }

    public static Float roundFourDigits(Float number) {
        return roundNDigits(number, 4);
    }

    private static Float roundFiveDigits(Float number) {
        return roundNDigits(number, 5);
    }

    public static Float roundNDigits(Float number, int digits) {
        return number == null ? null : new BigDecimal(number).setScale(digits, RoundingMode.HALF_UP).floatValue();
    }

    @Deprecated
    public static Float round(Float number, MathContext mc) {
        float old = number;
        float partieEntier = (int) old;
        float digit = old - (int) old;
        number = partieEntier + new BigDecimal(digit).round(mc).floatValue();
        return number;
    }

}
