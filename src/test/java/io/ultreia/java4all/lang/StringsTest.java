/*
 * #%L
 * Java Lang extends by Ultreia.io
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package io.ultreia.java4all.lang;

import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;

public class StringsTest {

    @Test
    public void testConvert() {
        Assert.assertEquals("365d", Strings.convertTime(31536000000000000L));
        Assert.assertEquals("2d", Strings.convertTime(172800000000000L));
        Assert.assertEquals("2h", Strings.convertTime(7200000000000L));
        Assert.assertEquals("2m", Strings.convertTime(120000000000L));

        Assert.assertEquals("2s", Strings.convertTime(2000000002L));
        Assert.assertEquals("2s", Strings.convertTime(2000000000L));
        Assert.assertEquals("2ms", Strings.convertTime(2000000L));
        Assert.assertEquals("2ns", Strings.convertTime(2L));
        Assert.assertEquals("0ns", Strings.convertTime(0L));

        Assert.assertEquals("0o", Strings.convertMemory(0L));
        Assert.assertEquals("2o", Strings.convertMemory(2L));
        Assert.assertEquals("2Ko", Strings.convertMemory(2048L));
        Assert.assertEquals("2Mo", Strings.convertMemory(2097152L));
        Assert.assertEquals("2Mo", Strings.convertMemory(2097154L));

        Assert.assertEquals("2Go", Strings.convertMemory(2147483648L));
        Assert.assertEquals("2To", Strings.convertMemory(2199023255552L));
        Assert.assertEquals("2000To", Strings.convertMemory(2199023255552000L));

        Assert.assertEquals("-2Mo", Strings.convertMemory(-2097152L));
        Assert.assertEquals("-2Mo", Strings.convertMemory(-2097154L));

        Locale oldLocale = Locale.getDefault();
        // test in french locale
        Locale.setDefault(Locale.FRENCH);
        Assert.assertEquals("2,02s", Strings.convertTime(2020000002L));
        Assert.assertEquals("2,094Mo", Strings.convertMemory(2196152L));
        Assert.assertEquals("-2,094Mo", Strings.convertMemory(-2196152L));

        // test in english locale
        Locale.setDefault(Locale.ENGLISH);
        Assert.assertEquals("2.02s", Strings.convertTime(2020000002L));
        Assert.assertEquals("2.094Mo", Strings.convertMemory(2196152L));
        Assert.assertEquals("-2.094Mo", Strings.convertMemory(-2196152L));

        // push back previous locale
        Locale.setDefault(oldLocale);
    }

    @Test
    public void testConvertToConstantName() {
        Assert.assertEquals("YES", Strings.convertToConstantName("yes"));
        Assert.assertEquals("YES", Strings.convertToConstantName("*$$?YEs"));
        Assert.assertEquals("YES", Strings.convertToConstantName("_yes!$*_"));
        Assert.assertEquals("YES", Strings.convertToConstantName("_Yes____"));

        Assert.assertEquals("YES_OR_NO", Strings.convertToConstantName("__yesOrNo_"));
        Assert.assertEquals("YES_OR_NO", Strings.convertToConstantName("Yes-or-!*=No"));
        Assert.assertEquals("YES_OR_NO", Strings.convertToConstantName("_yes__or__no"));
        Assert.assertEquals("YES_OR_NO", Strings.convertToConstantName("_YesOR___No"));
    }

    @Test
    public void testGetRelativeCamelCaseName() {
        Assert.assertEquals("Java4allLangStringsTest", Strings.getRelativeCamelCaseName("io.ultreia", StringsTest.class.getName()));
        Assert.assertEquals("LangStringsTest", Strings.getRelativeCamelCaseName("io.ultreia", StringsTest.class.getName(), "java4all"));
        Assert.assertEquals("LangStringsTest", Strings.getRelativeCamelCaseName("io.ultreia.java4all", StringsTest.class.getName()));

    }

}

