package io.ultreia.java4all.lang;

/*-
 * #%L
 * Java Lang extends by Ultreia.io
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.math.MathContext;

/**
 * Created on 09/03/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.5
 */
public class NumbersTest {

    @Test
    public void roundThreeDigits() {
        assertThreeDigits(1.5676099f, 1.568f, false);
        assertThreeDigits(456567.5676099f, 456567.568f, true);
        assertThreeDigits(4565671.5676099f, 4565671.568f, true);
    }

    private static void assertThreeDigits(float value, float expected, boolean same) {
        float actual = Numbers.roundThreeDigits(value);
        float bad = Numbers.round(value, new MathContext(3));
        Assert.assertEquals("" + expected, "" + actual);
        if (same) {
            Assert.assertEquals("" + expected, "" + bad);
        } else {
            Assert.assertNotEquals("" + expected, "" + bad);
        }
        Assert.assertEquals(expected, actual, 0.0000000001);
        if (same) {
            Assert.assertEquals(expected, bad, 0.0000000001);
        } else {
            Assert.assertNotEquals(expected, bad, 0.0000000001);

        }
    }
}
